terraform {
  backend "s3" {
    bucket = "hello-devops-8l"
    key    = "terraform.tfstate"
    region = "us-west-2"
  }
}

provider "aws" {
  version = "~> 2.48"
  region  = "us-west-2"
}

locals {
  name = "hello-devops"
}

data "aws_caller_identity" "current" {}

resource "aws_s3_bucket" "tf_backend" {
  bucket = "hello-devops-8l"
  acl    = "private"
}

resource "aws_lambda_function" "main" {
  function_name    = local.name
  filename         = "./lambda.zip"
  handler          = "lambda.handler"
  role             = aws_iam_role.main.arn
  runtime          = "python3.7"
  source_code_hash = filebase64sha256("./lambda.zip")
  publish          = true
}

resource "aws_iam_role" "main" {
  name = local.name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "cloudwatch" {
  role       = aws_iam_role.main.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_api_gateway_rest_api" "main" {
  name = local.name
}

resource "aws_api_gateway_resource" "main" {
  path_part   = "deploy"
  parent_id   = aws_api_gateway_rest_api.main.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_method" "main" {
  rest_api_id   = aws_api_gateway_rest_api.main.id
  resource_id   = aws_api_gateway_resource.main.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "main" {
  rest_api_id             = aws_api_gateway_rest_api.main.id
  resource_id             = aws_api_gateway_resource.main.id
  http_method             = aws_api_gateway_method.main.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.main.invoke_arn
}

resource "aws_lambda_permission" "main" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.main.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "arn:aws:execute-api:us-west-2:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.main.id}/*/${aws_api_gateway_method.main.http_method}${aws_api_gateway_resource.main.path}"
}

resource "aws_api_gateway_deployment" "main" {
  depends_on = [aws_api_gateway_integration.main]

  rest_api_id = aws_api_gateway_rest_api.main.id
  stage_name  = "demo"
}

output "url" {
  value = aws_api_gateway_deployment.main.invoke_url
}
